package com.wsln.libpam;


public abstract class AbsAction implements Action {
    public OnDismissListener mListener;

    @Override
    public abstract void show();

    @Override
    public void setOnDismissListener(OnDismissListener listener){
        mListener = listener;
    }
}
