package com.wsln.libpam;

import android.util.Log;

import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ActionManager {
    private String TAG = "---ActionQueueManager---";
    private static Queue<Action> queue = new ConcurrentLinkedQueue<>(); //弹窗队列(线程安全)
    private static volatile ActionManager sInstance;

    private ActionManager(){}

    public static ActionManager getInstance(){
        if (sInstance == null){
            synchronized (ActionManager.class){
                if (sInstance == null){
                    sInstance = new ActionManager();
                }
            }
        }
        return sInstance;
    }

    /**
     * 初始为0,pushToQueue 基数必然为1,所以小于2即可
     *
     * @return
     */
    public boolean canShow() {
        return queue.size() < 2/* && !isLock*/;
    }

    /**
     * 每次弹窗调用PushQueue方法
     *
     * @param action
     */
    public void pushToQueue(Action action) {
        //添加到队列中
        if (action != null) {
            action.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss() {
                    Log.e(TAG, "nextTask");
                    nextTask();
                }
            });
            Log.e(TAG, "add..");
            queue.add(action);
            //只有当前队列数量为1时才能进行下一步操作
            if (canShow()) {
                startNextIf();
            }
        }
    }

    /**
     * 显示下一个弹窗任务
     */
    private void startNextIf() {
        if (queue != null && queue.isEmpty()) {
            return;
        }
        //对弹窗进行排序
        sort();
        Action action = queue.element();
        if (action != null) {
            action.show();
        } else {
            Log.e(TAG, "任务队列为空...");
        }
    }

    private void sort() {

    }

    /**
     * 移除队列的头,获取最新队列头
     */
    private void removeTopTask() {
        queue.poll(); //出栈
    }

    /**
     * 提供外部下一个任务的方法,在弹窗消失时候调用
     */
    private void nextTask() {
        removeTopTask();
        startNextIf();
    }
}
