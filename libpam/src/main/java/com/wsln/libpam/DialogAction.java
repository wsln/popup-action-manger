package com.wsln.libpam;

import android.app.Dialog;
import android.content.DialogInterface;

public class DialogAction extends AbsAction {
    public Dialog mDialog;

    public DialogAction(Dialog mDialog) {
        this.mDialog = mDialog;
        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mListener.onDismiss();
            }
        });
    }

    @Override
    public void show() {
        if (mDialog != null){
            mDialog.show();
        }
    }
}
