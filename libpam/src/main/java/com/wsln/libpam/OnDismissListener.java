package com.wsln.libpam;

public interface OnDismissListener {
    void onDismiss();
}
