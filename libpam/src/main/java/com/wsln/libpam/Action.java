package com.wsln.libpam;

public interface Action {

    void setOnDismissListener(OnDismissListener listener);

    void show();
}
