package com.wsln.libpam;

import android.widget.PopupWindow;

import com.wsln.libpam.widget.CustomPopupWindow;


public class PopupWindowAction extends AbsAction {
    private CustomPopupWindow mPopupWindow;

    public PopupWindowAction(CustomPopupWindow popupWindow) {
        this.mPopupWindow = popupWindow;
        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mListener.onDismiss();
            }
        });
    }

    @Override
    public void show() {
        mPopupWindow.showAsDropDown();
    }
}
