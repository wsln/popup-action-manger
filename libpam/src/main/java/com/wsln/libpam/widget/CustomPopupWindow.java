package com.wsln.libpam.widget;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.wsln.libpam.utils.AppGlobals;
import com.wsln.libpam.utils.ScreenUtil;
import com.wsln.libpam.utils.ToastUtils;


/**
 * 解决android 7.0及以上的位置错乱bug
 */
public class CustomPopupWindow extends PopupWindow {
    private View mAnchor;

    public CustomPopupWindow(Context context) {
        super(context);
    }

    public CustomPopupWindow(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomPopupWindow(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomPopupWindow(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public CustomPopupWindow() {
    }

    public CustomPopupWindow(View contentView) {
        super(contentView);
    }

    public CustomPopupWindow(int width, int height) {
        super(width, height);
    }

    public CustomPopupWindow(View contentView, int width, int height) {
        super(contentView, width, height);
    }

    public CustomPopupWindow(View contentView, int width, int height, boolean focusable) {
        super(contentView, width, height, focusable);
    }

    @Override
    public void showAsDropDown(View anchor) {
        showAsDropDown(anchor,0,0,0);
    }

    @Override
    public void showAsDropDown(View anchor, int xoff, int yoff, int gravity) {

        // 7.0 以下或者高度为WRAP_CONTENT, 默认显示
        if (Build.VERSION.SDK_INT < 24 || getHeight() == ViewGroup.LayoutParams.WRAP_CONTENT) {
            showCompatSuper(anchor, xoff, yoff, gravity);
        } else {
            if (getContentView().getContext() instanceof Activity) {
                Activity activity = (Activity) getContentView().getContext();
                int screenHeight;
                // 获取屏幕真实高度, 减掉虚拟按键的高度
                screenHeight = ScreenUtil.getContentHeight(activity);
                int[] location = new int[2];
                // 获取控件在屏幕的位置
                anchor.getLocationOnScreen(location);
                // 算出popwindow最大高度
                int maxHeight = screenHeight - location[1] - anchor.getHeight();
                // popupwindow  有具体的高度值，但是小于anchor下边缘与屏幕底部的距离， 正常显示
                if(getHeight() > 0 && getHeight() < maxHeight){
                    showCompatSuper(anchor, xoff, yoff, gravity);
                }else {
                    // match_parent 或者 popwinddow的具体高度值大于anchor下边缘与屏幕底部的距离， 都设置为最大可用高度
                    setHeight(maxHeight);
                    showCompatSuper(anchor, xoff, yoff, gravity);
                }

            }
        }
    }

    /**
     * 设置锚点
     * @param anchor
     */
    public void setAnchor(View anchor){
        this.mAnchor = anchor;
    }

    public void showAsDropDown(){
        if (mAnchor == null){
            ToastUtils.show(AppGlobals.getApplication(),"must do setAnchor()");
        }else {
            showAsDropDown(mAnchor);
        }
    }

    // 解决andorid4.3及以下异常 java.lang.NoSuchMethodError: android.widget.PopupWindow.showAsDropDown
    private void showCompatSuper(View anchor, int xoff, int yoff, int gravity) {
        if (Build.VERSION.SDK_INT > 18)
            super.showAsDropDown(anchor, xoff, yoff, gravity);
        else
            super.showAsDropDown(anchor, xoff, yoff);
    }
}
