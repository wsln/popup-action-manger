# PopupActionManger

#### Popup Action Manger(PAM)

Use queue to access action data, and use strategy pattern to configure popupWindow and dialog.

#### Usage

java
```java
AlertDialog dialog1 = new AlertDialog.Builder(JavaActivity.this)
                        .setTitle("update")
                        .setIcon(R.mipmap.ic_launcher_round)
                        .setMessage("更新信息更新信息更新信息更新信息更新信息")
                        .setPositiveButton("确认", (dialog, which) -> dialog.dismiss())
                        .setNegativeButton("取消", (dialog, which) -> {
                            dialog.dismiss();
                        })
                        .create();
                DialogAction action1 = new DialogAction(dialog1);
                ActionManager.getInstance().pushToQueue(action1);

``` 

kotlin
```
val dialog1 = AlertDialog.Builder(this)
                .setTitle("kotlin测试")
                .setMessage("测试，测试，测试")
                .setPositiveButton(
                    "confirm"
                ) { dialog, which -> dialog.dismiss() }
                .setNegativeButton("cancel") { dialog, which ->
                    dialog.dismiss()
                }
                .create()
            var da1 = DialogAction(dialog1)
            PopActionManager.pullToQueue(da1)
```