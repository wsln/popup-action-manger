# PopupActionManger

#### 弹窗行为管理（PAM）

使用队列来存取行为(action)数据，使用策略模式来完成popupwindow和dialog的配置。

#### 使用方式

java
```java
AlertDialog dialog1 = new AlertDialog.Builder(JavaActivity.this)
                        .setTitle("update")
                        .setIcon(R.mipmap.ic_launcher_round)
                        .setMessage("更新信息更新信息更新信息更新信息更新信息")
                        .setPositiveButton("确认", (dialog, which) -> dialog.dismiss())
                        .setNegativeButton("取消", (dialog, which) -> {
                            dialog.dismiss();
                        })
                        .create();
                DialogAction action1 = new DialogAction(dialog1);
                ActionManager.getInstance().pushToQueue(action1);

``` 

kotlin
```
val dialog1 = AlertDialog.Builder(this)
                .setTitle("kotlin测试")
                .setMessage("测试，测试，测试")
                .setPositiveButton(
                    "confirm"
                ) { dialog, which -> dialog.dismiss() }
                .setNegativeButton("cancel") { dialog, which ->
                    dialog.dismiss()
                }
                .create()
            var da1 = DialogAction(dialog1)
            PopActionManager.pullToQueue(da1)
```

