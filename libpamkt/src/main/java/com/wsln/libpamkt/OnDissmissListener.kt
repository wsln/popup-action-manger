package com.wsln.libpamkt

interface OnDissmissListener {
    fun onDismiss()
}