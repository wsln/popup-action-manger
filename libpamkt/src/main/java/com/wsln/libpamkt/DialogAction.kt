package com.wsln.libpamkt

import android.app.Dialog

class DialogAction(dialog: Dialog) : AbsAction() {
    private var mDialog: Dialog = dialog

    init {
        dialog.setOnDismissListener { mListener.onDismiss() }
    }

    override fun show() {
        mDialog.show()
    }
}