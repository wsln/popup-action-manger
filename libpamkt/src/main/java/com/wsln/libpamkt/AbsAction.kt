package com.wsln.libpamkt

abstract class AbsAction : Action {
    lateinit var mListener:OnDissmissListener

    override fun setOnDismissListener(listener: OnDissmissListener) {
        mListener = listener
    }

    abstract override fun show()
}