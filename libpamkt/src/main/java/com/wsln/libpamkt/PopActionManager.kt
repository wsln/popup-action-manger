package com.wsln.libpamkt

import java.util.concurrent.ConcurrentLinkedDeque

object PopActionManager {
    private val queue by lazy {
        ConcurrentLinkedDeque<Action>()
    }

    private fun canShow(): Boolean {
        return queue.size < 2
    }

    fun pullToQueue(action: Action){
        action.setOnDismissListener(object : OnDissmissListener {
            override fun onDismiss() {
                nextTask()
            }
        })
        queue.add(action)
        if (canShow()){
            startNextIf()
        }
    }

    private fun startNextIf() {
        if (queue.isEmpty()){
            return
        }
        val action = queue.element()
        action.show()
    }

    fun removeTopTask(){
        queue.poll()
    }

    fun nextTask() {
        removeTopTask()
        startNextIf()
    }
}