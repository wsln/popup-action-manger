package com.wsln.libpamkt

interface Action {

    fun setOnDismissListener(listener: OnDissmissListener);

    fun show();

}