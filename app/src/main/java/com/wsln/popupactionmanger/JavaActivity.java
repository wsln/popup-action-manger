package com.wsln.popupactionmanger;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.wsln.libpam.ActionManager;
import com.wsln.libpam.DialogAction;
import com.wsln.libpam.PopupWindowAction;
import com.wsln.libpam.widget.CustomPopupWindow;
import com.wsln.popupactionmanger.databinding.ActivityJavaBinding;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class JavaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityJavaBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_java);
        new Handler(getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {

                AlertDialog dialog1 = new AlertDialog.Builder(JavaActivity.this)
                        .setTitle("update")
                        .setIcon(R.mipmap.ic_launcher_round)
                        .setMessage("更新信息更新信息更新信息更新信息更新信息")
                        .setPositiveButton("确认", (dialog, which) -> dialog.dismiss())
                        .setNegativeButton("取消", (dialog, which) -> {
                            dialog.dismiss();
                        })
                        .create();
                DialogAction action1 = new DialogAction(dialog1);
                ActionManager.getInstance().pushToQueue(action1);

                AlertDialog dialog2 = new AlertDialog.Builder(JavaActivity.this)
                        .setTitle("privacy")
                        .setIcon(R.mipmap.ic_launcher_round)
                        .setMessage("隐私信息隐私信息银禧信息")
                        .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("取消", (dialog, which) -> {
                            dialog.dismiss();
                        })
                        .create();
                DialogAction action2 = new DialogAction(dialog2);
                ActionManager.getInstance().pushToQueue(action2);


                Disposable disposable1 = Observable.just("dialog3").delay(10, TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<String>() {
                            @Override
                            public void accept(String s) throws Exception {
                                AlertDialog dialog3 = new AlertDialog.Builder(JavaActivity.this)
                                        .setTitle("delay")
                                        .setIcon(R.mipmap.ic_launcher_round)
                                        .setMessage("延迟信息，10秒延迟")
                                        .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .setNegativeButton("取消", (dialog, which) -> {
                                            dialog.dismiss();
                                        })
                                        .create();
                                DialogAction action3 = new DialogAction(dialog3);
                                ActionManager.getInstance().pushToQueue(action3);
                            }
                        });

                Disposable disposable2 = Observable.just("dialog4").delay(9, TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<String>() {
                            @Override
                            public void accept(String s) throws Exception {
                                CustomPopupWindow popupWindow = new CustomPopupWindow(JavaActivity.this);
                                View view = LayoutInflater.from(JavaActivity.this).inflate(R.layout.pop_privacy, null, false);
                                popupWindow.setContentView(view);
                                TextView confirm = view.findViewById(R.id.tv_confirm);
                                TextView cancel = view.findViewById(R.id.tv_cancel);
                                confirm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        popupWindow.dismiss();
                                    }
                                });
                                cancel.setOnClickListener(v -> {
                                    popupWindow.dismiss();
                                });
                                popupWindow.setAnchor(binding.tvAnchor);
                                PopupWindowAction pwa2 = new PopupWindowAction(popupWindow);
                                ActionManager.getInstance().pushToQueue(pwa2);

                            }
                        });
            }
        }, 1000);
    }
}