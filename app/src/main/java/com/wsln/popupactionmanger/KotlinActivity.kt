package com.wsln.popupactionmanger

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AlertDialog
import com.wsln.libpamkt.DialogAction
import com.wsln.libpamkt.PopActionManager

class KotlinActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kotlin)

        Handler(Looper.getMainLooper()).postDelayed({
            val dialog1 = AlertDialog.Builder(this)
                .setTitle("kotlin测试")
                .setMessage("测试，测试，测试")
                .setPositiveButton(
                    "confirm"
                ) { dialog, which -> dialog.dismiss() }
                .setNegativeButton("cancel") { dialog, which ->
                    dialog.dismiss()
                }
                .create()
            var da1 = DialogAction(dialog1)
            PopActionManager.pullToQueue(da1)

            val dialog2 = AlertDialog.Builder(this)
                .setTitle("update测试")
                .setMessage("update测试update测试update测试update测试")
                .setPositiveButton(
                    "confirm"
                ) { dialog, which -> dialog.dismiss() }
                .setNegativeButton("cancel") { dialog, which ->
                    dialog.dismiss()
                }
                .create()
            var da2 = DialogAction(dialog2)
            PopActionManager.pullToQueue(da2)
        }, 1000)
    }
}